const { validCandidate } = require('./functions');

describe('validCandidate', function() {
  it('should validate script', () => {
    expect(validCandidate('script')).toBe(true);
  });
  
  it('should validate ES3 string', () => {
    expect(validCandidate('ES3')).toBe(true);
  });
  
  it('should not validate ESa string', () => {
    expect(validCandidate('ESa')).toBe(false);
  });
  
  it('should not validate ES string', () => {
    expect(validCandidate('ES')).toBe(false);
  });
  
  it('should not validate a random string', () => {
    expect(validCandidate('blah')).toBe(false);
  });
  
  it('should not validate a number', () => {
    expect(validCandidate(2)).toBe(false);
  });
  
  it('should validate array with script in it', () => {
    expect(validCandidate(['script'])).toBe(true);
  });
  
  it('should validate array with ES2018 in it', () => {
    expect(validCandidate(['ES2018'])).toBe(true);
  });
  
  it('should not validate array with ESw in it', () => {
    expect(validCandidate(['ESw'])).toBe(false);
  });
  
  it('should not validate array with only a random string in it', () => {
    expect(validCandidate(['blah'])).toBe(false);
  });
  
  it('should not validate array with only random values in it', () => {
    expect(validCandidate(['blah1', 6])).toBe(false);
  });
  
  it('should validate an array with at least one script element', () => {
    expect(validCandidate(['blah1', 4, 'script'])).toBe(true);
  });
  
  it('should not validate an object as a parameter', () => {
    expect(validCandidate({ value: 'string' })).toBe(false);
  });
});
