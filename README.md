# Upflowy Coding Challenge
### Part 1: Software Programming

1. What does `validCandidate` expect and return?
  It expects any value and it returns a boolean

2. Tell us the reason(s) why we've created a variable to store the regex?
  It's cleaner to define it once because it avoids problems when you later need to change the regex.

3. Does it matter to use `==` or `===` when checking if `typeof languages` is equal to 'string' ?
  Not in this case because the type of `typeof` is string.

4. Rewrite `filter` so it doesn't use `=>`. What does `=>` do? Was it necessary in this instance?
   1. Code updated in `functions.js`
   2. The arrow function provides with a shorter sintax to define anonymous functions.
   3. Compared to an anonymous function, it behaves differently around the "this" keyword. One refers to the caller and the other to the owner of the function. We are not using "this" in here, so it makes no difference in that aspect.

5. Update `validCandidate` to accept candidates knowing "ES2015", "ES6" or "ES7". Basically ES whatever. By the way, what does "ES" stand for?
   1. Code updated in `functions.js`
   1. ES stands for EcmaScript. It is a JavaScript standard.

6. Write `validCandidate` unit tests
  Tests implemented in `functions.test.js`

### Part 2: Software Architecture
Definition: a Flow is a data structure that allows our system to handle steps and the transitions between them.

1. Given that a Flow has many versions, that at most one version can be active at any time and that our clients must have the ability to schedule in advance the activation or deactivation of versions:

   1. Could you describe how you would implement your NoSQL database to cover those functional requirements? Any drawing tool can be used to draw the database schema
    ```
      flowDB: {
        accounts: [accountID]: { name: <str>, flows: [flowID], activeFlow: <flowID> },
        flows: [flowID]: { flowVersion: <str>, active: <bool>, activationDate: <date>, deactivationDate: <date>, steps: [stepID], ... },
        steps: [stepID]: {...}
      }
    ```

   2. Could you describe the signature of the methods necessary to support the given functionalities “activate” and “deactivate”
  `const activateFlow = (accountID: number, flowID: number, activationDate?: date, deactivationDate?: date): any => {};`
    Assumptions:
      - The system should validate the dates with all the flows.
      - If the activation date is lower than the deactivation date of the current flow, on user confirmation, we update the current flow deactivation date to match the activation date on the new flow so the system can know when to replace the flow.
      - If there's no activation date, the current flow should be replaced with the one given.

      `const deactivateFlow = (accountID: number, flowID: number, deactivationDate?: date): any => {};`
    Assumptions:
      - If there's no deactivation date, the flow should be deactivated using the current date.
      - If the flow deactivated is the active one and there's no deactivationDate, the system should replace the flow with the next one or the default one.

1. Implement the necessary Typescript interfaces to describe the models and the methods 
  ...
1. Assuming we create an Isomorphic wrapper around the Firebase Cloud Firestore, how would we go to avoid security breaches? Point me towards the right documentation.
 Not sure about this one, but I would say that using application & API restrictions with browser and server keys, and only allowing trusted URLs to work with those keys. I haven't worked with isomorphic webapps


### Part 3: Software Operability
1. Choose at least one metric that you would use to monitor Flows’ performances.
    - Time spent on a flow
    - Time between options
    - Time between steps
    - Drop rate in between steps
    - Amount of clicks

1. What would you monitor in order to be aware of any unexpected behaviours?
  If by "unexpected behaviours" we are talking about problems with the system, environment, etc., I would implement some health monitoring and telemetry system along with a logging and tracing system if it's a microservices architecture. Custom boards to monitor the KPIs of the system is always a good idea too.

1. What practices would you put in place to prevent issues to be reappearing?
  Automated unit, integration or E2E testing on PRs, dogfooding, etc. In a complex microservices architecture I will also implement Chaos monkey.

1. What would you monitor in order to anticipate potential bottlenecks or scalability issues?
  If the system is gonna have a heavy usage, you should do an analysis on the data and calculate the workload of your servers, volume of data, amount of transactions, busy hours and potential peaks. Having that in mind, you might decide to go with one or another architecture, define different sizes for your system and data instances, etc. Depending on the architecture, you could go with plain monitoring of the servers and set alerts on CPU, RAM, network trafic, etc. In cloud computing, you could use autoscale features in your instances/orchestrators or just use cloud functions.

1. Who owns the responsibility to set the standard in software quality?
  The sofware quality is a shared responsibility. The development team should be always accountable for their code, sticking to some "defininition of done", implementing best practices, practicing pair programing, mob programing, participate in design sessions, code reviews, automated tests, etc. Have a product manager/owner which is involved in the whole SDLC is also a must.

