function validCandidate(languages) {
  let validCandidate = false
  let languageRegex  = /script|ES\d+$/
  if(Array.isArray(languages)) {
    validCandidate = !!languages.filter(function (language) { return languageRegex.test(language) }).length
  } else if (typeof languages == 'string') {
    validCandidate = languageRegex.test(languages)
  }
  return validCandidate
}

exports.validCandidate = validCandidate;
